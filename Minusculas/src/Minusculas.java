import java.util.Scanner;

public class Minusculas {

    public static void main(String[] args) throws OpcionNoValida {

        Scanner sc = new Scanner(System.in);
        String linea;

        do {
            linea = sc.nextLine();
            try {
                if (linea.matches("[+-]?\\d*(\\.\\d+)?")) throw new OpcionNoValida();
                System.out.println(linea.toLowerCase());

            } catch (OpcionNoValida e) {
                System.err.println(e.getMessage());
            }
        } while (!linea.equalsIgnoreCase("finalizar"));
    }
}
