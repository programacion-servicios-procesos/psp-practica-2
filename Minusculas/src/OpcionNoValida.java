

public class OpcionNoValida extends Exception{
    public OpcionNoValida(){
        super("Por favor, introduce sólo letras\");\n");
    }

    @Override
    public String getMessage() {
        String errorMessage = "¡No pongas números!";
        return errorMessage;
    }
}