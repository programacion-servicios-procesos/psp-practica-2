import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejercicio3 {

    public static void main(String[] args) {
//        System.out.println("Parámetros pasados al padre: ");
//        Arrays.stream(args).sequential().forEach(s -> System.out.print(s + " "));
//        System.out.println("");

        List<String> parametros = new ArrayList<>();

        parametros.add("java");
        parametros.add("-jar");
        parametros.add("../Minusculas/Minusculas.jar");

        ProcessBuilder pb = new ProcessBuilder(parametros);

        pb.redirectErrorStream(true);

        System.out.println("Escribe algo y te lo convertiré a minúsculas");
        System.out.println("");

        try {
            Process minusculas = pb.start(); //iniciar proceso
            minusculas.waitFor(2, TimeUnit.SECONDS);

            Scanner minusculasScanner = new Scanner(minusculas.getInputStream()); //Scanner lee el outputstream del otro programa

            OutputStream os = minusculas.getOutputStream(); //envío de palabras al proceso
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();

            while (!linea.equalsIgnoreCase("finalizar")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println(minusculasScanner.nextLine());
                linea = sc.nextLine();
            }
            System.out.println("");
            System.out.println("¡Hasta luego!");

        } catch (IOException ex) {
            Logger.getLogger(Ejercicio3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}