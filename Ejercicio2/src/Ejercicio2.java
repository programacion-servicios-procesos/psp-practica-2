import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejercicio2 {
    public static void main(String[] args) {

        System.out.println("Parámetros pasados al padre: ");
        Arrays.stream(args).sequential().forEach(s -> System.out.print(s + " "));
        System.out.println("");

        List<String> parametros = new ArrayList<>();

        parametros.add("java");
        parametros.add("-jar");
        parametros.add("../random10/out/artifacts/random10_jar/random10.jar");

        ProcessBuilder pb = new ProcessBuilder(parametros); // creamos processbuilder con la lista de argumentos

        //pb.inheritIO();

        System.out.println("Escribe algo y me inventaré un número del 1 al 10. Escribe stop para salir");

        try {
            Process random10 = pb.start(); //iniciar proceso
            random10.waitFor(2, TimeUnit.SECONDS);

            Scanner random10sc = new Scanner(random10.getInputStream()); //Scanner lee el outputstream del otro programa

            OutputStream os = random10.getOutputStream(); //envío de palabras al proceso
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();

            while (!linea.equals("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                System.out.println(random10sc.nextLine());
                linea = sc.nextLine();
            }
            System.out.println("¡Hasta luego!");

        } catch (IOException ex) {
            Logger.getLogger(Ejercicio2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
