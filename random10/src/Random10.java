import java.util.Scanner;

public class Random10 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String linea;

        do {
            linea = sc.nextLine();

            try {
                int numeroAleatorio = (int) (Math.random() * 10);
                System.out.println("Toma un número: " + numeroAleatorio);

            } catch (NumberFormatException e) {
                System.out.println("La línea introducida no es un número");
            }

        } while (!linea.equalsIgnoreCase("stop"));
    }
}

